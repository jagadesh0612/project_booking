package com.booking.config;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.booking.model.Auth;
import com.booking.repository.AuthRepo;

@Service
public class ApplicationUserDetailsService implements UserDetailsService {

	@Autowired
	AuthRepo authRepo;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Auth auth = authRepo.findByUsername(username);
		return new User(auth.getUsername(), auth.getPassword(), new ArrayList<>());
	}

}
