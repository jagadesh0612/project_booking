package com.booking.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.booking.filter.JWTRequestFilter;

@EnableWebSecurity
@Configuration
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

	@Autowired
	ApplicationUserDetailsService userDetails;

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		/*
		 * http.addFilterBefore(jwtAuthenticationFiler(),
		 * UsernamePasswordAuthenticationFilter.class) .csrf() .disable()
		 * .exceptionHandling().and().sessionManagement().sessionCreationPolicy(
		 * SessionCreationPolicy.STATELESS).and() .authorizeRequests()
		 * .antMatchers(HttpMethod.POST, "/api/loginUser").permitAll()
		 * .antMatchers(HttpMethod.POST, "/api/register").permitAll()
		 * .anyRequest().authenticated();
		 */
		/*
		 * http.csrf().disable().exceptionHandling().and().sessionManagement().
		 * sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
		 * .authorizeRequests() .antMatchers("/**").permitAll()
		 * .antMatchers("/resources/**").permitAll() .antMatchers("/js/**").permitAll()
		 * .antMatchers("/css/**").permitAll()
		 * .anyRequest().authenticated().and().httpBasic();
		 * http.addFilterBefore(jwtAuthenticationFiler(),
		 * UsernamePasswordAuthenticationFilter.class);
		 */

		http.csrf().disable().authorizeRequests()
		.antMatchers("/v2/api-docs", "/configuration/**", "/swagger*/**", "/webjars/**","/swagger-resources/**").permitAll()
		.antMatchers("/api/user/**").permitAll()
		.anyRequest().authenticated()
				.and().httpBasic().and().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);

		http.addFilterBefore(jwtAuthenticationFiler(), UsernamePasswordAuthenticationFilter.class);

	}

	@Bean
	public JWTRequestFilter jwtAuthenticationFiler() {
		return new JWTRequestFilter();
	}

	@Bean
	public BCryptPasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

	@Bean
	@Override
	public AuthenticationManager authenticationManagerBean() throws Exception {
		return super.authenticationManagerBean();
	}

/*	@Autowired
	public void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userDetails).passwordEncoder(passwordEncoder());
	}*/
	
	@Override
	protected void configure(final AuthenticationManagerBuilder auth) throws Exception {
	    auth.userDetailsService(userDetails)
	        .passwordEncoder(passwordEncoder());
	}
	 

}
