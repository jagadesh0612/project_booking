package com.booking.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import com.booking.exception.RoomException;
import com.booking.model.Activity;
import com.booking.model.Booking;
import com.booking.model.BookingResponse;
import com.booking.model.Response;
import com.booking.model.Room;
import com.booking.repository.RoomRepo;
import com.booking.repository.SequenceRepo;
import com.booking.repository.TransactionsRepo;
import com.booking.service.BookingService;

@Service
public class BookingServiceImpl implements BookingService {

	@Autowired
	BookingService bookingService;

	@Autowired
	RoomRepo roomRepo;

	@Autowired
	SequenceRepo sequence;

	@Autowired
	TransactionsRepo transactionsRepo;

	@Override
	public Response addRoom(Room room) {
		Response response = new Response();
		try {
			if (roomRepo.existsById(room.getId())) {
				throw new RoomException("Room already exists");
			} else {
				roomRepo.save(room);
				response.setResponseCode("0");
				response.setResponseMessage("Success!!!");
				return response;
			}
		} catch (RoomException e) {
			response.setResponseCode("1");
			response.setResponseMessage(e.getMessage());
			return response;
		}
	}

	@Override
	public List<Room> listRoom() {
		return roomRepo.findAll();
	}

	@Override
	public Booking bookRoom(Booking transactions, Authentication authentication) {
		transactions.setActivities(new ArrayList<Activity>());
		transactions.getRoomId().forEach(room -> {
			try {
				Room currentRoom = roomRepo.findById(room.getId())
						.orElseThrow(() -> new RoomException("Invalid Room Id"));
				try {
					if (currentRoom.isOccupied()) {
						throw new RoomException("Room is occupied");
					} else {
						currentRoom.setOccupied(true);
						roomRepo.save(currentRoom);
						transactions.getActivities().add(new Activity("BOOKING", "Success", room.getId(), new Date()));
					}
				} catch (RoomException e) {
					transactions.getActivities().add(new Activity("FAILED", e.getMessage(), room.getId(), new Date()));
				}
			} catch (Exception e) {
				transactions.getActivities().add(new Activity("FAILED", e.getMessage(), room.getId(), new Date()));
			}
		});
		transactions.setUserId(authentication.getName());
		transactions.setBookingTime(new Date());
		transactionsRepo.save(transactions);
		transactions.setRoomId(null);
		return transactions;
	}

	@Override
	public Response checkOutRoom(Long transaction_id, Long room_id) {
		Response response = new Response();
		try {
			Booking transaction = transactionsRepo.findById(transaction_id)
					.orElseThrow(() -> new RoomException("Invalid Transactionid"));
			Room room = roomRepo.findById(room_id).orElseThrow(() -> new RoomException("Invalid Room id"));
			room.setOccupied(false);
			roomRepo.save(room);
			transaction.getActivities().add(new Activity("CHECKOUT", "Success", room_id, new Date()));
			transactionsRepo.save(transaction);
			response.setResponseCode("0");
			response.setResponseMessage("Success!!");
		} catch (Exception e) {
			response.setResponseCode("1");
			response.setResponseMessage(e.toString());
		}
		return response;
	}

	@Override
	public String checkRoomCount() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public BookingResponse transactionDetails(Long transaction_id) {
		BookingResponse response = new BookingResponse();
		try {
			Booking transaction = transactionsRepo.findById(transaction_id)
					.orElseThrow(() -> new RoomException("Invalid transaction id"));
			response.setResponseCode("1");
			response.setResponseMessage("Success");
			response.setBookingDetails(transaction);
		} catch (RoomException e) {
			response.setResponseCode("1");
			response.setResponseMessage(e.getMessage());
		}
		return response;
	}

}
