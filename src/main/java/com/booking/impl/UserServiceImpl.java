package com.booking.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.booking.exception.UserException;
import com.booking.model.Auth;
import com.booking.model.Response;
import com.booking.repository.AuthRepo;
import com.booking.service.UserService;
import com.booking.utils.JwtUtils;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	AuthenticationManager authenticationManager;

	@Autowired
	UserDetailsService userDetails;

	@Autowired
	JwtUtils jwtUtils;

	@Autowired
	AuthRepo authRepo;

	@Autowired
	BCryptPasswordEncoder encoder;

	@Override
	public Response loginUser(Auth auth) {
		authenticationManager
				.authenticate(new UsernamePasswordAuthenticationToken(auth.getUsername(), auth.getPassword()));
		userDetails.loadUserByUsername(auth.getUsername());
		String token = jwtUtils.generateJwtToken(auth.getUsername());
		Response response = new Response();
		response.setResponseCode("0");
		response.setResponseMessage(token);
		return response;
	}

	@Override
	public Response registerUser(Auth auth) {
		Response response = new Response();
		try {
			if (!authRepo.existsById(auth.getUsername())) {
				auth.setPassword(encoder.encode(auth.getPassword()));
				authRepo.insert(auth);
				response.setResponseCode("0");
				response.setResponseMessage("User Created Successfully.");
				return response;
			} else {
				throw new UserException("User Already Exists.");
			}
		} catch (Exception e) {
			response.setResponseCode("1");
			response.setResponseMessage(e.toString());
		}
		return response;
	}

}
