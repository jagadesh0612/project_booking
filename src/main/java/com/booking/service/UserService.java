package com.booking.service;

import com.booking.model.Auth;
import com.booking.model.Response;

public interface UserService {
	public Response loginUser(Auth auth);
	public Response registerUser(Auth auth);
}
