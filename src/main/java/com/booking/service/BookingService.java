package com.booking.service;

import java.util.List;

import org.springframework.security.core.Authentication;

import com.booking.model.Booking;
import com.booking.model.BookingResponse;
import com.booking.model.Response;
import com.booking.model.Room;

public interface BookingService {
	public Response addRoom(Room room);
	public List<Room> listRoom();
	public Booking bookRoom(Booking transactions, Authentication auth);
	public Response checkOutRoom(Long id, Long room_id);
	public String checkRoomCount();
	public BookingResponse transactionDetails(Long transaction_id);
}
