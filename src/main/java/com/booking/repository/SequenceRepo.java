package com.booking.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.booking.model.Sequence;

public interface SequenceRepo extends MongoRepository<Sequence, Integer> {
	Sequence findByName(String name);
}
