package com.booking.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.booking.model.Booking;

public interface TransactionsRepo extends MongoRepository<Booking, Long>{

}
