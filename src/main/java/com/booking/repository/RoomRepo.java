package com.booking.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.booking.model.Room;

public interface RoomRepo extends MongoRepository<Room, Long> {

}
