package com.booking.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.booking.model.Auth;

public interface AuthRepo extends MongoRepository<Auth, String> {
	Auth findByUsername(String username);
	
}
