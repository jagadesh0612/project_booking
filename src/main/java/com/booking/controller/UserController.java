package com.booking.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.booking.model.Auth;
import com.booking.model.Response;
import com.booking.service.UserService;

@RestController
@RequestMapping("/api/user")
public class UserController {

	@Autowired
	UserService userService;

	@GetMapping("/ping")
	public String ping() {
		String s = "User Controller is responding : " + System.currentTimeMillis();
		return s;
	}

	@PostMapping("/signup")
	public ResponseEntity<Response> registerUser(@RequestBody Auth auth) {
		Response res = userService.registerUser(auth);
		if(res.getResponseCode().equals("0")) {
			return ResponseEntity.ok().body(res);
		}else {
			return ResponseEntity.badRequest().body(res);
		}
	}

	@PostMapping("/login")
	public ResponseEntity<Response> loginUser(@RequestBody Auth auth) {
		return ResponseEntity.ok(userService.loginUser(auth));
	}
}
