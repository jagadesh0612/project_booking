/**
 * 
 */
package com.booking.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.booking.model.Booking;
import com.booking.model.BookingResponse;
import com.booking.model.Response;
import com.booking.model.Room;
import com.booking.service.BookingService;

@RestController
@RequestMapping("/api/room")
public class BookingController {

	@Autowired
	BookingService bookingService;
	
	@PostMapping("add")
	public ResponseEntity<Response> addRoom(@RequestBody Room room) {
		Response res = bookingService.addRoom(room);
		if (res.getResponseCode().equals("0"))
			return ResponseEntity.ok().body(res);
		else
			return ResponseEntity.badRequest().body(res);
	}

	@GetMapping("/show")
	public List<Room> listRoom() {
		return bookingService.listRoom();
	}

	@PostMapping("/book")
	public ResponseEntity<Booking> bookRoom(@RequestBody Booking transactions, Authentication auth) {
		Booking book = bookingService.bookRoom(transactions, auth);
		return ResponseEntity.ok().body(book);
	}

	@PostMapping("/checkout")
	public ResponseEntity<Response> checkOutRoom(@RequestParam Long transaction_id, @RequestParam Long room_id) {
		Response res = bookingService.checkOutRoom(transaction_id, room_id);
		if (res.getResponseCode().equals("0"))
			return ResponseEntity.ok().body(res);
		else
			return ResponseEntity.badRequest().body(res);
	}
	
	@PostMapping("/transactiondetails")
	public ResponseEntity<BookingResponse> transactionDetails(@RequestParam Long transaction_id) {
		BookingResponse res = bookingService.transactionDetails(transaction_id);
		if (res.getResponseCode().equals("0"))
			return ResponseEntity.ok().body(res);
		else
			return ResponseEntity.badRequest().body(res);
	}
}
