package com.booking.exception;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
public class RoomException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	

	private String message;

	public String toString() {
		return message;
	}

}
