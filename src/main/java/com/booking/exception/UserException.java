package com.booking.exception;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
public class UserException extends Exception {

	private static final long serialVersionUID = -6198270167027800585L;

	private String message;

	public String toString() {
		return message;
	}

}
