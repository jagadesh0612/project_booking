package com.booking.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Getter;
import lombok.Setter;

@Document
public class Room {
	@Id
	@Getter @Setter long id;
	@Getter @Setter String description;
	@Getter @Setter boolean isOccupied;
}
