package com.booking.model;

import org.springframework.data.annotation.Id;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
public class Auth {
	@Id @Getter @Setter @NonNull private String username;
	@Getter @Setter @NonNull private String password;
	@Getter @Setter @NonNull private String firstName;
	@Getter @Setter @NonNull private String lastName;
	@Getter @Setter @NonNull private String emailAddress;
}
