package com.booking.model;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
public class Activity {
	@Getter @Setter String name;
	@Getter @Setter String description;
	@Getter @Setter long roomId;
	@Getter @Setter Date createdAt;
}
