package com.booking.model;

import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Getter;
import lombok.Setter;

@Document
public class Sequence {
	@Getter @Setter String name;
	@Getter @Setter long counter;
}
