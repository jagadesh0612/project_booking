package com.booking.model;

import lombok.Getter;
import lombok.Setter;

public class BookingResponse {
	
	@Getter @Setter String responseCode;
	@Getter @Setter String responseMessage;
	@Getter @Setter Booking bookingDetails;

	public BookingResponse() {
		// TODO Auto-generated constructor stub
	}
}
