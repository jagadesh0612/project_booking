package com.booking.model;

import java.util.Date;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

@Document
public class Booking {
	@Id
	@Getter @Setter long id;
	@Getter @Setter List<Room> roomId;
	@Getter @Setter String userId;
	@Getter @Setter Date bookingTime;
	@Getter @Setter @NonNull Date startDate;
	@Getter @Setter @NonNull Date endDate;
	@Getter @Setter String description;
	@Getter @Setter List<Activity> activities;
}
