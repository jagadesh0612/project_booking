# E-Booking
A Spring Boot application for Booking Hotels

# Technologies
- Java 8
- Spring Boot 
- Spring Security
- Maven
- MongoDB

# To run the service
- Configure the mongodb host in the application.properties file
- Build the service with maven
- Run the service
	- java -jar booking-0.0.1-SNAPSHOT.jar